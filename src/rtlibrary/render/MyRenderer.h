// Copyright 2009-2020 Intel Corporation
// SPDX-License-Identifier: Apache-2.0

#pragma once

//#include "MyRenderer.ih"
#include "ospray/SDK/render/Material.h"
#include "ospray/SDK/render/Renderer.h"

namespace ospray {
namespace rtlibrary {

struct MyRenderer : public Renderer
{
    MyRenderer();
  //virtual ~MyRenderer() override = default;
  virtual std::string toString() const override;
  void endFrame (FrameBuffer *fb, void *perFrameData) override;

};

} // namespace rtlibrary
} // namespace ospray
