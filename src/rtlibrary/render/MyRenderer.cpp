// Copyright 2009-2020 Intel Corporation
// SPDX-License-Identifier: Apache-2.0

#include "MyRenderer.h"
// ispc exports
#include "MyRenderer_ispc.h"

namespace ospray {
namespace rtlibrary {

MyRenderer::MyRenderer()
{
  ispcEquivalent = ispc::MyRenderer_create(this);
};

std::string MyRenderer::toString() const
{
  return "ospray::MyRenderer";
}

void MyRenderer::endFrame (FrameBuffer *fb, void *perFrameData) {};


} // namespace rtlibrary
} // namespace ospray
